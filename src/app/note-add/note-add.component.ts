import { Component, OnInit } from '@angular/core';
import { Note } from '../models/note';
import { NotesServiceService } from '../services/notes-service.service';

@Component({
  selector: 'app-note-add',
  templateUrl: './note-add.component.html',
  styleUrls: ['./note-add.component.css']
})
export class NoteAddComponent implements OnInit {

  note : Note;

  constructor(private service: NotesServiceService) { 
    this.note=new Note();
  }

  ngOnInit(): void {
  }

  onSubmit(e:Event){
    e.preventDefault();
    this.service.addNote(this.note);
    this.note = new Note();
  }

}

