import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesList2Component } from './notes-list2.component';

describe('NotesList2Component', () => {
  let component: NotesList2Component;
  let fixture: ComponentFixture<NotesList2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotesList2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesList2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
