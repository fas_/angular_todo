import { Component, OnInit } from '@angular/core';
import { Note } from '../models/note';
import { NotesServiceService } from '../services/notes-service.service';

@Component({
  selector: 'app-notes-list2',
  templateUrl: './notes-list2.component.html',
  styleUrls: ['./notes-list2.component.css']
})
export class NotesList2Component implements OnInit {

  notes : Note[];
  selectedNote:Note;

  constructor(private notesService: NotesServiceService) { }

  ngOnInit(): void {
    this.notesService.notes.subscribe((notes) =>{ this.notes = notes;
    this.selectedNote=null;
    console.log(JSON.stringify(notes))}, (err) => { console.log(err); });
  }

  selectNote(note:Note){
    this.selectedNote=note;
  }

  removeNote(index:number): void{
    this.notesService.deleteNote(index);
  }

}

