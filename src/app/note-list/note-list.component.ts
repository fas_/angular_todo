import { Component, OnInit, Input } from '@angular/core';
import { Note } from '../models/note';
import { NotesServiceService } from '../services/notes-service.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NotesDialogComponent } from '../notes-dialog/notes-dialog.component';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent {

  notes : Note[];

  constructor(private notesService: NotesServiceService, public dialog: MatDialog) {  
  }

  ngOnInit(): void {
    this.notesService.notes.subscribe((notes) =>{ this.notes = notes;
    console.log(JSON.stringify(notes))}, (err) => { console.log(err); });
  }

  openDialog(index:number): void{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
       note:this.notes[index]
    };
    const dialogRef = this.dialog.open(NotesDialogComponent,dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });  
  }

  removeNote(index:number): void{
    this.notesService.deleteNote(index);
  }

}