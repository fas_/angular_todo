import { Component, Inject } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { Note } from '../models/note';

@Component({
  selector: 'app-notes-dialog',
  templateUrl: './notes-dialog.component.html',
  styleUrls: ['./notes-dialog.component.css']
})
export class NotesDialogComponent  {

  note :Note;

  constructor(private dialogRef: MatDialogRef<NotesDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { 
    this.note = data.note;
    console.log(this.note);
  }

  save(): void{
    this.dialogRef.close('closed');
  }

  close(): void{
    this.dialogRef.close();
  }

}

export interface DialogData {
  note: Note;
}

