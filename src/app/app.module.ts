import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';


import {MatGridListModule} from '@angular/material/grid-list';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoteAddComponent } from './note-add/note-add.component';
import { NoteListComponent } from './note-list/note-list.component';
import { NotesFullComponent } from './notes-full/notes-full.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotesDialogComponent } from './notes-dialog/notes-dialog.component';
import { NotesDetailsComponent } from './notes-details/notes-details.component';
import { NotesList2Component } from './notes-list2/notes-list2.component';

@NgModule({
  declarations: [
    AppComponent,
    NoteAddComponent,
    NoteListComponent,
    NotesFullComponent,
    NotesDialogComponent,
    NotesDetailsComponent,
    NotesList2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule
  ],
  entryComponents: [
    NotesDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
