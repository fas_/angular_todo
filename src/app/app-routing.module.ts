import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotesFullComponent } from './notes-full/notes-full.component';
import { NotesList2Component } from './notes-list2/notes-list2.component';


const routes: Routes = [
  {
    path: '',
    component: NotesFullComponent
  },
  {
    path: 'list',
    component: NotesList2Component
  },
  {
    path: '**',
    component: NotesFullComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
